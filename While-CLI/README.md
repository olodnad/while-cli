# An Extremely Simple Interpreter for While and Loop Programs

## While and Loop

For more info about the While and Loop programming languages see the respective (German) Wikipedia entries: [While](https://de.wikipedia.org/wiki/WHILE-Programm), [Loop](https://de.wikipedia.org/wiki/LOOP-Programm)

## Usage

1. Starting the application, you will be prompted to specify the path to a source file. Enter the path to a text file containing some Loop or While code. The path should be entered between quotes, e.g. `"/home/foo/bar/myProgram.loop"`.
2. If the path specified links to a valid program, the application will respond with a pretty printed output of your code.
3. Next, you are prompted to specify the input parameters to run the program. Inputs are given by comma-separated-values, again between quotes, e.g. `"15,7"`.
4. Upon successfully running your code with the input provided, you will be shown the result and asked if you want to continue. If you answer `"n"` the program is terminated; otherwise, it starts again.