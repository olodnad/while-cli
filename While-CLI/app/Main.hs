module Main where

import Control.Monad (void)
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

import Syntax
import Semantics
import Pretty
import Parsing

parseProgram :: IO (Maybe Program)
parseProgram = do
  putStrLn "Path to source file:"
  path <- readLn
  s <- readFile path
  case parse progP "" s of
    Right p -> do
        putStrLn $ concat
            [ "Sucessfully parsed program:\n"
            , pretty 0 p
            ]
        return $ Just p
    Left err -> do
        putStrLn $ concat
            [ "Invalid input: "
            , show err
            ]
        return Nothing

parseInput :: IO [Integer]
parseInput = do
    putStrLn "Specify input:"
    inp <- readLn
    case parse inputP "" inp of
        Right xs -> do
            putStrLn "Successfully parsed input:"
            putStrLn $ prettyInput xs
            return xs
        Left err -> do
            putStrLn $ "Invalid input: " ++ show err
            return []

main = do
    mp <- parseProgram
    case mp of
        Nothing -> main
        Just p -> do
            input <- parseInput
            putStrLn "Computation result (x0):"
            putStrLn $ show $ run p input
            putStrLn $ "Continue?"
            answer <- readLn
            case head answer of
                'n' -> return ()
                _   -> main
