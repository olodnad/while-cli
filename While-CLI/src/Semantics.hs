module Semantics where

import Syntax
import qualified Data.Map.Strict as MS

-------------------------------------------------------------------------------
-- | Semantics
-------------------------------------------------------------------------------

-- | The semantics/meaning of "running a Loop program" is in terms instructions
-- | that change the values held by the variables 'x0,x1...'. Thus, a Loop
-- | is interpreted as a function to update the following state.
type State = MS.Map Int Integer

-- | Variables in Loop are initiated to 0 by default.
getVar :: Int -> State -> Integer
getVar i m = case MS.lookup i m of
    Nothing -> 0
    Just x  -> x


-- | Evaluating a literal to a natural number.
-- | Non initiated variables are defaulted to 0.
evalLit :: State -> Literal -> Integer
evalLit m (X i) = getVar i m
evalLit _ (C x) = x

-- | To evaluate terms, the involved literals
--  are evaluated and then processed according
--  to the given operations. Negative values will
--  default to 0.
evalTerm :: State -> Term -> Integer
evalTerm m (Plus l1 l2) = evalLit m l1 + evalLit m l2
evalTerm m (Minus l1 l2) = max 0 $ evalLit m l1 - evalLit m l2


-- | Evaluating a Loop program with respect to the standard semantics:
--  - 'Empty' does nothing
--  - 'Assignment i term' assigns the value of term to the (i'th) variable xi.
--  - 'Loop lit sub' iterates the (evaluation) of the subroutine exactly the
--    value of lit many times.
--  - 'Seq p1 p2' runs first p1 and then p2
eval :: Program -> State -> State
eval p m = case p of
    Empty -> m
    Assignment i t -> MS.insert i (evalTerm m t) m
    Loop l sub -> iter (evalLit m l) (eval sub) m
    While i sub -> while i (eval sub) m
    Seq p1 p2 -> case eval p1 m of
        m' -> eval p2 m'
    where
        iter :: Integer -> (a -> a) -> a -> a
        iter 0 _ x = x
        iter n f x = iter (n-1) f $ f x

        while :: Int -> (State -> State) -> State -> State
        while i f s
            | getVar i s == 0 = s
            | otherwise = case f s of
                s' -> while i f s'

-- | Running a loop program means to initiate the state with the given
-- | input in the variables x1... and then returning the content of the
-- | variable x0 as the result of the computation.
run :: Program -> [Integer] -> Integer
run p input = case MS.lookup 0 final of
    Nothing -> 0
    Just x -> x
    where
        initial = MS.fromList $ zip [1..] input
        final = eval p initial
